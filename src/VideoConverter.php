<?php

namespace Hermes\VideoConverter;

use Storage;
use Exception;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class VideoConverter
{
    // The command to access FFMPEG, since this could differ from windows to ubuntu servers it's variable
    private $ffmpegExecutable;

    // Constructor
    public function __construct()
    {
        $this->ffmpegExecutable = config("video-converter.ffmpeg_executable");
    }

    /**
     * Set FMMPEG executable path
     * 
     * @param       string              Path to the executable
     * @return      void
     */
    public function setFmmpegExecutable($executablePath)
    {
        $this->ffmpegExecutable = $executablePath;
    }
    
    /**
     * Get FMMPEG executable path
     * 
     * @return      string
     */
    public function getFmmpegExecutable()
    {
        return $this->ffmpegExecutable;
    }

    /**
     * Convert video
     * 
     * @param       string              Input file's path
     * @param       string              Input file's format (without the dot; so 'avi', 'webm')
     * @param       string              Desired output file format
     * @param       string              Desired output file storage path
     * @param       string              Desired output file name (optional; will be randomly generated if left empty)
     * @return      string              Filepath to the converted video
     * @throws      Exception
     */
    public function convertVideo($input_file_path, $input_format, $output_format, $storage_path, $output_file_name = null)
    {
        // Determine the final output file path
        $output_file_path = $storage_path . "/" . $this->generateFilename($output_format, $output_file_name);

        // Determine the absolute file path
        $absolute_output_file_path = storage_path("app") . "/" . $output_file_path;

        // Generate conversion command
        $command = $this->generateConversionCommand($input_file_path, $input_format, $absolute_output_file_path, $output_format);

        // Run the command to convert the video
        try
        {
            $process = new Process($command);
            $process->mustRun();
        }
        catch (ProcessFailedException $e)
        {
            throw new Exception("Failed to convert video: ".$e->getMessage());
        }

        // Return the relative output file path
        return $output_file_path;
    }

    /**
     * Crop media (audio/video)
     * 
     * @param       string              Input file's path
     * @param       string              Input file's format
     * @param       integer             Start from X seconds
     * @param       integer             Stop at X seconds
     * @param       string              Desired output file storage path
     * @param       string              Desired output file name (optional)
     * @return      string              Filepath to the converted video
     */
    public function cropMedia($input_file_path, $input_format, $from, $to, $storage_path, $output_file_name = null)
    {
        // Determine the final output file path
        $output_file_path = $storage_path . "/" . $this->generateFilename($input_format, $output_file_name);

        // Determine the absolute file path
        $absolute_output_file_path = storage_path("app") . "/" . $output_file_path;

        // Generate the crop command
        $command = $this->generateCropCommand($input_file_path, $input_format, $absolute_output_file_path, $from, $to);

        // Run the command to crop the video
        try
        {
            $process = new Process($command);
            $process->mustRun();
        }
        catch (Exception $e)
        {
            throw new Exception("Failed to crop video: ".$e->getMessage());
        }

        // Return the relative output file path
        return $output_file_path;
    }

    /**
     * Generate conversion command
     * 
     * @return      
     */
    public function generateConversionCommand($input_file_path, $input_format, $output_file_path, $output_format)
    {
        // Switch between supported input file formats
        switch ($input_format)
        {
            case "mp4":

                // Switch between supported output file formats
                switch ($output_format)
                {
                    case "mp3":
                        $command = $this->ffmpegExecutable." -i ".$input_file_path." -q:a 0 -map a ".$output_file_path;
                    break;

                    case "webm":
                        $command = $this->ffmpegExecutable." -i ".$input_file_path." -c:v libvpx -crf 10 -b:v 1M -c:a libvorbis ".$output_file_path;
                    break;
                    
                    default: throw new Exception("No support for generating command to convert from ".$input_format." to ".$output_format.".");
                }

            break;
            
            case "webm":

                // Switch between supported output file formats
                switch ($output_format)
                {
                    case "mp3":
                        $command = $this->ffmpegExecutable." -i ".$input_file_path." -vn -ab 128k -ar 44100 -y ".$output_file_path;
                    break;

                    default: throw new Exception("No support for generating command to convert from ".$input_format." to ".$output_format.".");
                }

            break;

            default: throw new Exception("No support for input format ".$input_format);
        }

        return $command;
    }

    /**
     * Generate crop command
     * 
     * @param           string                  Path to the input file
     * @param           string                  Format (extension) of the in- and output file
     * @param           string                  Path to output file
     * @param           string                  Start cropping from ... (in seconds)
     * @param           string                  Stop cropping at ... (in seconds)
     * @return          string                  The command to crop
     */
    public function generateCropCommand($input_file_path, $format, $output_file_path, $from, $to)
    {
        // Convert seconds to HH:MM:SS timestamps
        $from_timestamp = $this->generateTimestampFromSeconds($from);
        $to_timestamp = $this->generateTimestampFromSeconds($to);

        // Switch between supported input file formats
        switch ($format)
        {
            case "mp3":
            case "mp4":       
                return $this->ffmpegExecutable." -i ".$input_file_path." -ss ".$from_timestamp." -to ".$to_timestamp." -c copy ".$output_file_path;
            break;
            
            default: throw new Exception("No support for the output format: ".$format);
        }
    }

    /**
     * Generate (random) filename
     * 
     * @param       string              $extension of the output file
     * @param       string              $filename (optional)
     * @return      string
     */
    public function generateFilename($extension, $file_name = null)
    {
        $random_string = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1).substr(md5(time()),1);
        return is_null($file_name) ? $random_string.".".$extension : $file_name."_".$random_string.".".$extension;
    }

    /**
     * Generate timestamp (HH:MM:SS) from seconds
     * 
     * @param       integer                         Seconds
     * @return      string                          Timestamp          
     */
    public function generateTimestampFromSeconds($seconds)
    {
        $t = round($seconds);
        return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
    }
}