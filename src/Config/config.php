<?php

/**
 * Video Converter configuration file
 * 
 * @author      Nick Verheijen <verheijen.webdevelopment@gmail.com>
 * @version     1.0.0
 */

return [

    'ffmpeg_executable' => 'ffmpeg'
    
];