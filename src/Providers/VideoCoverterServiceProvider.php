<?php

namespace Hermes\VideoConverter\Providers;

use Illuminate\Support\ServiceProvider;
use Hermes\VideoConverter\VideoConverter;

class VideoConverterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootstrapConfig();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('video-converter', function() {
            return new VideoConverter;
        });
    }

    /**
     * Bootstrap config(uration)
     * 
     * @return      void
     */
    private function bootstrapConfig()
    {
        // Setup config merging
        $this->mergeConfigFrom(__DIR__."/../Config/config.php", "video-converter");

        // Setup config file publishing
        $this->publishes([__DIR__."/../Config/config.php" => config_path("video-converter.php")]);
    }

}