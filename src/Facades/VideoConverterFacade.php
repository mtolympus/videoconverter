<?php

namespace Hermes\VideoConverter\Facades;

use Illuminate\Support\Facades\Facade;

class VideoConverterFacade extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'video-converter';
    }
}
